/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gamme
 */
public class RecieptDetail {
    private int id;
    private int productId;
    private String productName;
    private float productPrice;
    private int qty;
    private float total_price;
    private int receiptId;

    public RecieptDetail(int id, int productId, String productName, float productPrice, int qty, float total_price, int receiptId) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.total_price = total_price;
        this.receiptId = receiptId;
    }
    public RecieptDetail(int productId, String productName, float productPrice, int qty, float total_price, int receiptId) {
        this.id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.total_price = total_price;
        this.receiptId = receiptId;
    }
    public RecieptDetail() {
        this.id = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.qty = 0;
        this.total_price = 0;
        this.receiptId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(float total_price) {
        this.total_price = total_price;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int receiptId) {
        this.receiptId = receiptId;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + ", total_price=" + total_price + ", receiptId=" + receiptId + '}';
    }
        public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("receipt_detail_id"));
            recieptDetail.setProductId(rs.getInt("product"));
            recieptDetail.setProductName(rs.getString("product-name"));
            recieptDetail.setProductPrice(rs.getFloat("product-price"));
           recieptDetail.setQty(rs.getInt("aty"));
           recieptDetail.setTotal_price(rs.getFloat("total_price"));
           recieptDetail.setProductId(rs.getInt("reciept_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }
}
